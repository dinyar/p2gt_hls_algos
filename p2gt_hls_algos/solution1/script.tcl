############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 1986-2018 Xilinx, Inc. All Rights Reserved.
############################################################
open_project p2gt_hls_algos
set_top p2gt_condChecks_HLS
add_files src/p2gt_doubleObjCond.cc -cflags "-std=c++11"
add_files -tb tb/p2gt_doubleObjCond_tb.cc -cflags "-Wno-unknown-pragmas"
open_solution "solution1"
set_part {xcvu9p-fsgd2104-2-i}
create_clock -period 480MHz -name default
config_compile  
config_export -format ip_catalog -rtl vhdl -vivado_phys_opt place -vivado_report_level 0
config_schedule -effort medium  -relax_ii_for_timing=0 
config_bind -effort medium
config_sdx -optimization_level none -target none
set_clock_uncertainty 50%
#source "./p2gt_hls_algos/solution1/directives.tcl"
csim_design
csynth_design
cosim_design -rtl vhdl
export_design -flow impl -rtl vhdl -format ip_catalog
