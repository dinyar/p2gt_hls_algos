# ==============================================================
# File generated on Thu Apr 01 17:48:22 CEST 2021
# Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
# SW Build 2405991 on Thu Dec  6 23:36:41 MST 2018
# IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
# Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
# ==============================================================
add_files -tb ../../tb/p2gt_doubleObjCond_tb.cc -cflags { -Wno-unknown-pragmas}
add_files src/p2gt_doubleObjCond.cc -cflags -std=c++11
set_part xcvu9p-fsgd2104-2-i
create_clock -name default -period 480MHz
set_clock_uncertainty 25% default
config_compile -no_signed_zeros=0
config_compile -unsafe_math_optimizations=0
config_export -format=ip_catalog
config_export -rtl=vhdl
config_export -vivado_phys_opt=place
config_export -vivado_report_level=0
config_schedule -effort=medium
config_schedule -enable_dsp_full_reg=0
config_schedule -relax_ii_for_timing=0
config_schedule -verbose=0
config_bind -effort=medium
config_sdx -optimization_level=none
config_sdx -target=none
