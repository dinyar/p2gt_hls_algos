#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>

#include "../include/p2gt_doubleObjCond.hh"

int main(int argc, char *argv[]) {
  bool fast_fail{false};

  std::ifstream testfile("../../../../tb/dummy_testfile.dat");
  if (!testfile.is_open()) {
    std::cout << "Test file couldn't be opened." << std::endl;
    return -1;
  }

  int errCnt{0};
  int caseNum{0};
  while (testfile.peek() != EOF) {
    ++caseNum;

    std::string case_description;
    pt_t pT1_cut{0};
    bool pT1_cut_active{false};
    pt_t pT2_cut{0};
    bool pT2_cut_active{false};
    eta_t minEta1_cut{0};
    bool minEta1_cut_active{false};
    eta_t maxEta1_cut{0};
    bool maxEta1_cut_active{false};
    eta_t minEta2_cut{0};
    bool minEta2_cut_active{false};
    eta_t maxEta2_cut{0};
    bool maxEta2_cut_active{false};
    phi_t minPhi1_cut{0};
    bool minPhi1_cut_active{false};
    phi_t maxPhi1_cut{0};
    bool maxPhi1_cut_active{false};
    phi_t minPhi2_cut{0};
    bool minPhi2_cut_active{false};
    phi_t maxPhi2_cut{0};
    bool maxPhi2_cut_active{false};
    dz_t minDz1_cut{0};
    bool minDz1_cut_active{false};
    dz_t maxDz1_cut{0};
    bool maxDz1_cut_active{false};
    dz_t minDz2_cut{0};
    bool minDz2_cut_active{false};
    dz_t maxDz2_cut{0};
    bool maxDz2_cut_active{false};
    qual_t qual1_cut{0};
    bool qual1_cut_active{false};
    qual_t qual2_cut{0};
    bool qual2_cut_active{false};
    iso_t iso1_cut{0};
    bool iso1_cut_active{false};
    iso_t iso2_cut{0};
    bool iso2_cut_active{false};
    eta_t dEtaMin_cut{0};
    bool dEtaMin_cut_active{false};
    eta_t dEtaMax_cut{0};
    bool dEtaMax_cut_active{false};
    phi_t dPhiMin_cut{0};
    bool dPhiMin_cut_active{false};
    phi_t dPhiMax_cut{0};
    bool dPhiMax_cut_active{false};
    correlation_t dRSquaredMin_cut{0};
    bool dRSquaredMin_cut_active{false};
    correlation_t dRSquaredMax_cut{0};
    bool dRSquaredMax_cut_active{false};
    correlation_t invMassDiv2Min_cut{0};
    bool invMassDiv2Min_cut_active{false};
    correlation_t invMassDiv2Max_cut{0};
    bool invMassDiv2Max_cut_active{false};
    hls::stream<Object> test_objects1, test_objects2;
    bool exp_res{true};

    std::string l;
    bool gotDescription{false};
    bool gotCuts{false};
    int gotCollection1Num{0};
    int gotCollection2Num{0};
    while (std::getline(testfile, l)) {
      if (l.rfind("#", 0) == 0 || l.length() == 0) {
        continue;
      }
      std::istringstream ss(l);
      if (!gotDescription) {
        case_description = l;
        gotDescription = true;
      } else if (!gotCuts) {
        if (ss >> std::boolalpha >> pT1_cut >> pT1_cut_active >> pT2_cut >> pT2_cut_active >> minEta1_cut >>
            minEta1_cut_active >> maxEta1_cut >> maxEta1_cut_active >> minEta2_cut >> minEta2_cut_active >>
            maxEta2_cut >> maxEta2_cut_active >> minPhi1_cut >> minPhi1_cut_active >> maxPhi1_cut >>
            maxPhi1_cut_active >> minPhi2_cut >> minPhi2_cut_active >> maxPhi2_cut >> maxPhi2_cut_active >>
            minDz1_cut >> minDz1_cut_active >> maxDz1_cut >> maxDz1_cut_active >> minDz2_cut >> minDz2_cut_active >>
            maxDz2_cut >> maxDz2_cut_active >> qual1_cut >> qual1_cut_active >> qual2_cut >> qual2_cut_active >>
            iso1_cut >> iso1_cut_active >> iso2_cut >> iso2_cut_active >> dEtaMin_cut >> dEtaMin_cut_active >>
            dEtaMax_cut >> dEtaMax_cut_active >> dPhiMin_cut >> dPhiMin_cut_active >> dPhiMax_cut >>
            dPhiMax_cut_active >> dRSquaredMin_cut >> dRSquaredMin_cut_active >> dRSquaredMax_cut >>
            dRSquaredMax_cut_active >> invMassDiv2Min_cut >> invMassDiv2Min_cut_active >> invMassDiv2Max_cut >>
            invMassDiv2Max_cut_active) {
          gotCuts = true;
        } else {
          std::cout << "Problem getting cuts from string: " << ss.str() << std::endl;
          std::cout << "Get " << pT1_cut << " and " << pT1_cut_active << std::endl;
          return -1;
        }
      } else if (gotCollection1Num < 12) {
        Object tmp;
        if (ss >> std::boolalpha >> tmp.hwPt >> tmp.hwEta >> tmp.hwPhi >> tmp.hwDz >> tmp.hwIso >> tmp.hwQual >>
            tmp.hwCharge >> tmp.valid) {
          ++gotCollection1Num;
          test_objects1.write(tmp);
        } else {
          std::cout << "Problem getting coll1" << std::endl;
          return -1;
        }
      } else if (gotCollection2Num < 12) {
        Object tmp;
        if (ss >> std::boolalpha >> tmp.hwPt >> tmp.hwEta >> tmp.hwPhi >> tmp.hwDz >> tmp.hwIso >> tmp.hwQual >>
            tmp.hwCharge >> tmp.valid) {
          ++gotCollection2Num;
          test_objects2.write(tmp);
        } else {
          std::cout << "Problem getting coll2" << std::endl;
          return -1;
        }
      } else {
        if (ss >> std::boolalpha >> exp_res) {
          break;  // Done with this test case.
        } else {
          std::cout << "Problem getting expected result" << std::endl;
          return -1;
        }
      }
    }

    // Call function under test and store return values
    bool res = p2gt_condChecks_HLS(pT1_cut,
                                   pT1_cut_active,
                                   pT2_cut,
                                   pT2_cut_active,
                                   minEta1_cut,
                                   minEta1_cut_active,
                                   maxEta1_cut,
                                   maxEta1_cut_active,
                                   minEta2_cut,
                                   minEta2_cut_active,
                                   maxEta2_cut,
                                   maxEta2_cut_active,
                                   minPhi1_cut,
                                   minPhi1_cut_active,
                                   maxPhi1_cut,
                                   maxPhi1_cut_active,
                                   minPhi2_cut,
                                   minPhi2_cut_active,
                                   maxPhi2_cut,
                                   maxPhi2_cut_active,
                                   minDz1_cut,
                                   minDz1_cut_active,
                                   maxDz1_cut,
                                   maxDz1_cut_active,
                                   minDz2_cut,
                                   minDz2_cut_active,
                                   maxDz2_cut,
                                   maxDz2_cut_active,
                                   qual1_cut,
                                   qual1_cut_active,
                                   qual2_cut,
                                   qual2_cut_active,
                                   iso1_cut,
                                   iso1_cut_active,
                                   iso2_cut,
                                   iso2_cut_active,
                                   dEtaMin_cut,
                                   dEtaMin_cut_active,
                                   dEtaMax_cut,
                                   dEtaMax_cut_active,
                                   dPhiMin_cut,
                                   dPhiMin_cut_active,
                                   dPhiMax_cut,
                                   dPhiMax_cut_active,
                                   dRSquaredMin_cut,
                                   dRSquaredMin_cut_active,
                                   dRSquaredMax_cut,
                                   dRSquaredMax_cut_active,
                                   invMassDiv2Min_cut,
                                   invMassDiv2Min_cut_active,
                                   invMassDiv2Max_cut,
                                   invMassDiv2Max_cut_active,
                                   true,  // TODO: Also test with objects invalid?
                                   test_objects1,
                                   test_objects2);

    // Compare result to expected value here.
    if (res != exp_res) {
      std::cout << "Failed test case " << caseNum << ": " << case_description << "\n";
      std::cout << "Result:\t\t" << std::boolalpha << res << "\n";
      std::cout << "Expected:\t" << exp_res << std::endl;
      ++errCnt;
      if (fast_fail) {
        return errCnt;
      }
    } else {
      std::cout << "Passed test case " << caseNum << ": " << case_description << std::endl;
    }
  }

  return errCnt;
}
