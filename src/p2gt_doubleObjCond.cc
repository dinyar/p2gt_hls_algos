#include "../include/p2gt_doubleObjCond.hh"

//DEBUG!
// #include <iostream>

#include <cmath>

#include "hls_math.h"

// TODO: Deal with comparison of same collection.
bool p2gt_condChecks_HLS(const pt_t pT1_cut,
                         const bool pT1_cut_active,
                         const pt_t pT2_cut,
                         const bool pT2_cut_active,
                         const eta_t minEta1_cut,
                         const bool minEta1_cut_active,
                         const eta_t maxEta1_cut,
                         const bool maxEta1_cut_active,
                         const eta_t minEta2_cut,
                         const bool minEta2_cut_active,
                         const eta_t maxEta2_cut,
                         const bool maxEta2_cut_active,
                         const phi_t minPhi1_cut,
                         const bool minPhi1_cut_active,
                         const eta_t maxPhi1_cut,
                         const bool maxPhi1_cut_active,
                         const phi_t minPhi2_cut,
                         const bool minPhi2_cut_active,
                         const eta_t maxPhi2_cut,
                         const bool maxPhi2_cut_active,
                         const dz_t minDz1_cut,
                         const bool minDz1_cut_active,
                         const dz_t maxDz1_cut,
                         const bool maxDz1_cut_active,
                         const dz_t minDz2_cut,
                         const bool minDz2_cut_active,
                         const dz_t maxDz2_cut,
                         const bool maxDz2_cut_active,
                         const qual_t qual1_cut,
                         const bool qual1_cut_active,
                         const qual_t qual2_cut,
                         const bool qual2_cut_active,
                         const iso_t iso1_cut,
                         const bool iso1_cut_active,
                         const iso_t iso2_cut,
                         const bool iso2_cut_active,
                         const eta_t dEtaMin_cut,
                         const bool dEtaMin_cut_active,
                         const eta_t dEtaMax_cut,
                         const bool dEtaMax_cut_active,
                         const phi_t dPhiMin_cut,
                         const bool dPhiMin_cut_active,
                         const phi_t dPhiMax_cut,
                         const bool dPhiMax_cut_active,
                         const correlation_t dRSquaredMin_cut,
                         const bool dRSquaredMin_cut_active,
                         const correlation_t dRSquaredMax_cut,
                         const bool dRSquaredMax_cut_active,
                         const correlation_t invMassDiv2Min_cut,
                         const bool invMassDiv2Min_cut_active,
                         const correlation_t invMassDiv2Max_cut,
                         const bool invMassDiv2Max_cut_active,
                         const bool objects_valid,
                         hls::stream<Object>& objects1,
                         hls::stream<Object>& objects2) {
  // const bool objects_valid, Object objects1, Object objects2) {
  // #pragma HLS array_partition variable=object2 complete
#pragma HLS interface ap_ctrl_none port = return
#pragma HLS interface ap_stable port = pT1_cut
#pragma HLS interface ap_stable port = pT1_cut_active
#pragma HLS interface ap_stable port = pT2_cut
#pragma HLS interface ap_stable port = pT2_cut_active
#pragma HLS interface ap_stable port = minEta1_cut
#pragma HLS interface ap_stable port = minEta1_cut_active
#pragma HLS interface ap_stable port = maxEta1_cut
#pragma HLS interface ap_stable port = maxEta1_cut_active
#pragma HLS interface ap_stable port = minEta2_cut
#pragma HLS interface ap_stable port = minEta2_cut_active
#pragma HLS interface ap_stable port = maxEta2_cut
#pragma HLS interface ap_stable port = maxEta2_cut_active
#pragma HLS interface ap_stable port = minPhi1_cut
#pragma HLS interface ap_stable port = minPhi1_cut_active
#pragma HLS interface ap_stable port = maxPhi1_cut
#pragma HLS interface ap_stable port = maxPhi1_cut_active
#pragma HLS interface ap_stable port = minPhi2_cut
#pragma HLS interface ap_stable port = minPhi2_cut_active
#pragma HLS interface ap_stable port = maxPhi2_cut
#pragma HLS interface ap_stable port = maxPhi2_cut_active
#pragma HLS interface ap_stable port = minDz1_cut
#pragma HLS interface ap_stable port = minDz1_cut_active
#pragma HLS interface ap_stable port = maxDz1_cut
#pragma HLS interface ap_stable port = maxDz1_cut_active
#pragma HLS interface ap_stable port = minDz2_cut
#pragma HLS interface ap_stable port = minDz2_cut_active
#pragma HLS interface ap_stable port = maxDz2_cut
#pragma HLS interface ap_stable port = maxDz2_cut_active
#pragma HLS interface ap_stable port = qual1_cut
#pragma HLS interface ap_stable port = qual1_cut_active
#pragma HLS interface ap_stable port = qual2_cut
#pragma HLS interface ap_stable port = qual2_cut_active
#pragma HLS interface ap_stable port = iso1_cut
#pragma HLS interface ap_stable port = iso1_cut_active
#pragma HLS interface ap_stable port = iso2_cut
#pragma HLS interface ap_stable port = iso2_cut_active
#pragma HLS interface ap_stable port = dEtaMin_cut
#pragma HLS interface ap_stable port = dEtaMin_cut_active
#pragma HLS interface ap_stable port = dEtaMax_cut
#pragma HLS interface ap_stable port = dEtaMax_cut_active
#pragma HLS interface ap_stable port = dPhiMin_cut
#pragma HLS interface ap_stable port = dPhiMin_cut_active
#pragma HLS interface ap_stable port = dPhiMax_cut
#pragma HLS interface ap_stable port = dPhiMax_cut_active
#pragma HLS interface ap_stable port = dRSquaredMin_cut
#pragma HLS interface ap_stable port = dRSquaredMin_cut_active
#pragma HLS interface ap_stable port = dRSquaredMax_cut
#pragma HLS interface ap_stable port = dRSquaredMax_cut_active
#pragma HLS interface ap_stable port = invMassDiv2Min_cut
#pragma HLS interface ap_stable port = invMassDiv2Min_cut_active
#pragma HLS interface ap_stable port = invMassDiv2Max_cut
#pragma HLS interface ap_stable port = invMassDiv2Max_cut_active
#pragma HLS interface ap_hs port = objects1
#pragma HLS interface ap_hs port = objects2
#pragma HLS pipeline II = 12
#pragma HLS latency min = 35 max = 35

  // DEBUG
  // std::cout << "pt1 cut: " << pT1_cut << " pt2 cut: " << pT2_cut << std::endl;

  // Only start operating when objects become valid
  // TODO: Can use objects_valid signal as the "!empty" signal for the FIFOs.
  if (!objects_valid) {
    return false;
  }

  Object o1[12], o2[12];
  for (int i = 0; i < 12; ++i) {
    o1[i] = objects1.read();
    o2[i] = objects2.read();
    //		o1[i] = objects1;
    //		o2[i] = objects2;
  }

  // TODO: Would be better if we first initialise this to false, then OR it after each inner loop.
  bool iteration_res[12];
  // Quality and iso bits currently not used.
  for (int i1 = 0; i1 < 12; ++i1) {
	iteration_res[i1] = false;
    if (!o1[i1].valid) {
      continue;
    }
    static ap_int<5> cosMem[2048];
    p2gt_initCosMem(cosMem);
    static ap_int<17> coshMem[2048];
    p2gt_initCoshMem(coshMem);
    for (int i2 = 0; i2 < 12; ++i2) {
      //DEBUG
      // std::cout << "eta1: " << o1[i1].hwEta << " eta2: " << o2[i2].hwEta << std::endl;

      if (!o2[i2].valid) {
        continue;
      }

      bool local_res{true};
      local_res &= (!pT1_cut_active) || o1[i1].hwPt > pT1_cut;
      // DEBUG
      // std::cout << local_res << std::endl;
      local_res &= (!minEta1_cut_active) || o1[i1].hwEta > minEta1_cut;
      local_res &= (!maxEta1_cut_active) || o1[i1].hwEta < maxEta1_cut;
      local_res &= (!minPhi1_cut_active) || o1[i1].hwPhi > minPhi1_cut;
      local_res &= (!maxPhi1_cut_active) || o1[i1].hwPhi < maxPhi1_cut;
      local_res &= (!minDz1_cut_active) || o1[i1].hwDz > minDz1_cut;
      local_res &= (!maxDz1_cut_active) || o1[i1].hwDz < maxDz1_cut;
      // DEBUG
      // std::cout << local_res << std::endl;
      local_res &= (!pT2_cut_active) || o2[i2].hwPt > pT2_cut;
      // DEBUG
      // std::cout << local_res << std::endl;
      local_res &= (!minEta2_cut_active) || o2[i2].hwEta > minEta2_cut;
      local_res &= (!maxEta2_cut_active) || o2[i2].hwEta < maxEta2_cut;
      local_res &= (!minPhi2_cut_active) || o2[i2].hwPhi > minPhi2_cut;
      local_res &= (!maxPhi2_cut_active) || o2[i2].hwPhi < maxPhi2_cut;
      local_res &= (!minDz2_cut_active) || o2[i2].hwDz > minDz2_cut;
      local_res &= (!maxDz2_cut_active) || o2[i2].hwDz < maxDz2_cut;
      // DEBUG
      // std::cout << local_res << std::endl;

      eta_t dEta;
      if(o1[i1].hwEta > o2[i2].hwEta) {
    	  dEta = o1[i1].hwEta - o2[i2].hwEta;
      } else {
    	  dEta = o2[i2].hwEta - o1[i1].hwEta;
      }
      phi_t dPhi;
      if(o1[i1].hwPhi > o2[i2].hwPhi) {
    	  dPhi = o1[i1].hwPhi - o2[i2].hwPhi;
      } else  {
    	  dPhi = o2[i2].hwPhi - o1[i1].hwPhi;
      }
      // TODO: Try the code below with Vitis and bind_op
//      eta_t dEta = hls::abs(o1[i1].hwEta - o2[i2].hwEta);
//      phi_t dPhi = hls::abs(o1[i1].hwPhi - o2[i2].hwPhi);
      local_res &= (!dEtaMin_cut_active) || dEta > dEtaMin_cut;
      local_res &= (!dEtaMax_cut_active) || dEta < dEtaMax_cut;
      local_res &= (!dPhiMin_cut_active) || dPhi > dPhiMin_cut;
      local_res &= (!dPhiMax_cut_active) || dPhi < dPhiMax_cut;
      // TODO: This seems to go to the addition below without a register stage. Check how to change this!
//      int dEtaSquared = dEta * dEta;
//      #pragma HLS RESOURCE variable=dEtaSquared core=DSP48
//      int dPhiSquared = dPhi * dPhi;
//      #pragma HLS RESOURCE variable=dPhiSquared core=DSP48
//      int dRSquared = dEtaSquared + dPhiSquared;
      int dRSquared = dEta*dEta + dPhi*dPhi;
      #pragma HLS RESOURCE variable=dRSquared core=DSP48 latency=3
      local_res &= (!dRSquaredMin_cut_active) || dRSquared > dRSquaredMin_cut;
      local_res &= (!dRSquaredMax_cut_active) || dRSquared < dRSquaredMax_cut;
      int invMassDiv2 = o1[i1].hwPt * o2[i2].hwPt * (coshMem[dEta] - cosMem[dPhi]);
      local_res &= (!invMassDiv2Min_cut_active) || invMassDiv2 > invMassDiv2Min_cut;
      local_res &= (!invMassDiv2Max_cut_active) || invMassDiv2 < invMassDiv2Max_cut;
      // DEBUG
      // std::cout << local_res << std::endl;

      iteration_res[i1] |= local_res;
      // DEBUG
      // std::cout << "Res: " << res << std::endl;
    }
  }

  bool res{false};
  for (int i = 0; i < 12; ++i) {
    res |= iteration_res[i];
  }
  // DEBUG
  // std::cout << "Returning: " << res << std::endl;
  return res;

  // Stream in objects one by one
  // Store both in an array
  // Once we have all objects (i.e. after N_OBJECTS ticks) start comparison
  // function Compare Ith object of object1 collection with all objects from
  // object2 collection. Try if then telling the compiler that we have II=1
  // allows us to do this without further modifications.
}

void p2gt_initCosMem(ap_int<5> mem[]) {
  for (int i = 0; i < std::pow(2, 11); ++i) {
    int lut_output_raw = static_cast<int>(10 * std::cos(2 * 0.0025 * i) / 1);
    if (lut_output_raw < 0) {
      lut_output_raw += (1 << 5);
    }
    mem[i] = static_cast<ap_int<5> >(lut_output_raw & (1 << 5) - 1);
  }
}

void p2gt_initCoshMem(ap_int<17> mem[]) {
  for (int i = 0; i < std::pow(2, 11); ++i) {
    int lut_output_raw = static_cast<int>(10 * std::cosh(4 * 0.0025 * i) / 1);
    if (lut_output_raw < 0) {
      lut_output_raw += (1 << 17);
    }
    mem[i] = static_cast<ap_int<5> >(lut_output_raw & (1 << 17) - 1);
  }
}
