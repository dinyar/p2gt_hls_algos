#ifndef p2gt_doubleObjCond_h
#define p2gt_doubleObjCond_h

#include <string>
#include <sstream>

#include "ap_int.h"
#include "hls_stream.h"

// TODO: These widths should Eventually be centralised and be retrieved by HLS, HDL, and emulator code.
typedef ap_uint<14> pt_t;
typedef ap_int<12> eta_t;
typedef ap_uint<12> phi_t;
typedef ap_uint<12> dz_t;
typedef ap_uint<2> iso_t;
typedef ap_uint<4> qual_t;
typedef ap_uint<2> charge_t;
typedef ap_uint<24> correlation_t;

struct Object {
  pt_t hwPt;
  eta_t hwEta;
  phi_t hwPhi;
  dz_t hwDz;
  iso_t hwIso;
  qual_t hwQual;
  charge_t hwCharge;
  bool valid;
};

const int N_OBJECTS = 12;

bool p2gt_condChecks_HLS(const pt_t pT1_cut,
                         const bool pT1_cut_active,
                         const pt_t pT2_cut,
                         const bool pT2_cut_active,
                         const eta_t minEta1_cut,
                         const bool minEta1_cut_active,
                         const eta_t maxEta1_cut,
                         const bool maxEta1_cut_active,
                         const eta_t minEta2_cut,
                         const bool minEta2_cut_active,
                         const eta_t maxEta2_cut,
                         const bool maxEta2_cut_active,
                         const phi_t minPhi1_cut,
                         const bool minPhi1_cut_active,
                         const eta_t maxPhi1_cut,
                         const bool maxPhi1_cut_active,
                         const phi_t minPhi2_cut,
                         const bool minPhi2_cut_active,
                         const eta_t maxPhi2_cut,
                         const bool maxPhi2_cut_active,
                         const dz_t minDz1_cut,
                         const bool minDz1_cut_active,
                         const dz_t maxDz1_cut,
                         const bool maxDz1_cut_active,
                         const dz_t minDz2_cut,
                         const bool minDz2_cut_active,
                         const dz_t maxDz2_cut,
                         const bool maxDz2_cut_active,
                         const qual_t qual1_cut,
                         const bool qual1_cut_active,
                         const qual_t qual2_cut,
                         const bool qual2_cut_active,
                         const iso_t iso1_cut,
                         const bool iso1_cut_active,
                         const iso_t iso2_cut,
                         const bool iso2_cut_active,
                         const eta_t dEtaMin_cut,
                         const bool dEtaMin_cut_active,
                         const eta_t dEtaMax_cut,
                         const bool dEtaMax_cut_active,
                         const phi_t dPhiMin_cut,
                         const bool dPhiMin_cut_active,
                         const phi_t dPhiMax_cut,
                         const bool dPhiMax_cut_active,
                         const correlation_t dRSquaredMin_cut,
                         const bool dRSquaredMin_cut_active,
                         const correlation_t dRSquaredMax_cut,
                         const bool dRSquaredMax_cut_active,
                         const correlation_t invMassDiv2Min_cut,
                         const bool invMassDiv2Min_cut_active,
                         const correlation_t invMassDiv2Max_cut,
                         const bool invMassDiv2Max_cut_active,
                         const bool objects_valid,
                         hls::stream<Object>& objects1,
                         hls::stream<Object>& objects2);

void p2gt_initCosMem(ap_int<5> mem[]);

void p2gt_initCoshMem(ap_int<17> mem[]);

#endif
